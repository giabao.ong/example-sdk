package model

import (
	"gitlab.com/giabao.ong/example/sdk"
)

type Info struct {
	Id      string `json:"id,omitempty" bson:"_id,omitempty"`
	System  string        `json:"system" bson:"system,omitempty"`
	AppName string        `json:"appName" bson:"app_name,omitempty"`
	Size    int           `json:"size" bson:"size,omitempty"`
}

var Mail = sdk.SMTP{
	Receiver: []sdk.ReceiverInfo{
		{Email: "onggiabao1998@gmail.com",
			Content: "Hello",
			Title:   "Bé",
		},
	},
	ColLog: "mail_log",
	Sender: sdk.SenderInfo{
		Email:    "noreply.yuh@gmail.com",
		Content:  "",
		Name:     "HACKER",
		Password: "yuh@12345",
	},
}

type info struct {
	DB sdk.DBModel
}

var InfoModel = &info{DB: sdk.DBModel{
	TemplateObject: Info{},
	ColName:        "systems",
}}
