package action

import (
	"github.com/globalsign/mgo/bson"
	"gitlab.com/giabao.ong/example/model"
	"gitlab.com/giabao.ong/example/sdk"
)

func GetInfo() *sdk.Response {
	f := model.Info{
	}
	q := bson.M{}
	bytes, _ := bson.Marshal(f)
	bson.Unmarshal(bytes, &q)
	res := model.InfoModel.DB.Query(f,0,0,false)
	//res = model.InfoModel.DB.Delete(f)

	return res
}
