package main

import (
	"gitlab.com/giabao.ong/example/api"
	"gitlab.com/giabao.ong/example/model"
	"gitlab.com/giabao.ong/example/sdk"
	"time"
)

func main() {
	sys := sdk.System{}
	ser := &sdk.Server{
		Addr:       "",
		Port:       80,
		Protocol:   "",
		ServerName: "test",
	}

	ser2 := &sdk.Server{
		Addr:       "",
		Port:       81,
		Protocol:   "",
		ServerName: "test2",
	}

	ser3 := &sdk.Server{
		Addr:       "",
		Port:       82,
		Protocol:   "",
		ServerName: "test3",
	}

	tim := time.Duration(3 * time.Second)
	jwt := sdk.JWT{
		Exp:  &tim,
		Key:  "Baooooooooo",
		Hash: sdk.TypeAlg.HS256,
	}
	ar := make(map[string]interface{})
	ar["data"] = "hello"
	jwt.JWTEncoder(ar)

	err := sys.CreateServerWithHttp(ser)
	if err != nil {
		panic(err)
	}
	err = sys.CreateServerWithHttp(ser2)
	if err != nil {
		panic(err)
	}
	err= sys.CreateServerWithHttp(ser3)
	if err != nil {
		panic(err)
	}

	db1 := &sdk.ConfigDB{
		DBName: "test",
		Addr: "127.0.0.1",
	}

	err = sys.DBInit(db1, OnDBConnected)
	if err != nil {
		panic(err)
	}

	ser.Get("/a", api.GetInfo)
	ser.Get("/q", api.Check)
	ser.PublicDir("/static/", "./public")
	ser.PreRequest(api.Check)
	ser3.Get("/m",api.GetInfo)
	sys.Launch()
}

func OnDBConnected(session *sdk.DBSession) {
	model.InfoModel.DB.Init(session)
	model.Mail.Init(session)
}
